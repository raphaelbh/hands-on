package br.com.treinamento.dojo.controller;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.glassfish.jersey.client.ClientConfig;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class WelcomeController {

	private static final String PUBLIC_KEY = "03d5becdfa492743e495f023c4e031f7";
	private static final String PRIVATE_KEY = "8c4cf7e8b9b8d4694269d039723e8ea927e44d55";
	private static final String BASE_REST_PATH = "https://gateway.marvel.com:443/v1/public/";

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/helloworld", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> helloWorld() throws Exception {
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		
		WebTarget charactersTarget = client.target(getRestPath("characters"));
		String charactersResponse = charactersTarget.request().get(String.class).toString();
		
		WebTarget comicsTarget = client.target(getRestPath("comics"));
		String comicsResponse = comicsTarget.request().get(String.class).toString();
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> charactersResult = mapper.readValue(charactersResponse, Map.class);
		Map<String, Object> comicsResult = mapper.readValue(comicsResponse, Map.class);
		
		Map<String, Object> result = new HashMap<>();
		result.put("charactersResult", charactersResult);
		result.put("comicsResult", comicsResult);
		
		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
	}
	
	private String getRestPath(String service) throws Exception {
		
		String ts = new Long(new Date().getTime()).toString();
		String generatedHhash = generateHash(ts);//"ef238f6864ea7d65a2de89270c6dc970";
		String restPath = BASE_REST_PATH.concat(service).concat("?ts=").concat(ts).concat("&apikey=").concat(PUBLIC_KEY).concat("&hash=").concat(generatedHhash);
		
		return restPath;
	}

	private String generateHash(String ts) throws Exception {
		
		String key = ts.concat(PRIVATE_KEY).concat(PUBLIC_KEY);
		
		MessageDigest digest = MessageDigest.getInstance("MD5");
		digest.update(key.getBytes(), 0, key.length());
		
		return new BigInteger(1, digest.digest()).toString(16);
	}

}
